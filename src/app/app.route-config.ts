import { Routes } from '@angular/router';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
export const routerConfig: Routes = [
    {
        path:'',
        redirectTo:'aq-index.json',
        pathMatch:'full'
    },
    {
        path:'aq-index.json',
        loadChildren:'./home/home.module#HomeModule'
    },
    {
        path:'signup',
        component: SignupComponent
    },
    {
        path:'login',
        component: LoginComponent
    },
    {
        path:'**',
        redirectTo:'aq-index.json',
        pathMatch:'full'
    }
];
