import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { routerConfig } from './app.route-config';
import { UserBaseService } from './shared/services/user-base.service';

import {APP_BASE_HREF} from '@angular/common';
import { LocationService } from './shared/services/location.service';
import { StorageService } from './shared/services/storage.service';
import { SharedModule } from './shared/shared.module';


@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    LoginComponent,
  ],
  imports: [
    HttpModule,
    HttpClientModule,
    SharedModule,
    RouterModule.forRoot(routerConfig),
  ],
  exports:[BrowserModule,FormsModule] ,
  providers: [UserBaseService,StorageService,{provide: APP_BASE_HREF, useValue : '/'},LocationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
