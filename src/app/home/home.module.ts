import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BookingComponent } from './components/booking/booking.component';
import { CommonModule } from '@angular/common';
import { PaymentService } from '../shared/services/payment.service';
import { AuthGuardService as AuthGuard, AuthGuardService } from './../shared/auth/auth-guard.service';
import { AuthService } from './../shared/auth/auth.service';
import { SharedModule } from '../shared/shared.module';
import { MylistComponent } from './components/mylist/mylist.component';
const routerConfig = [{
  path: '',
  component: HomeComponent,
},
{
  path: 'booking',
  component: BookingComponent,
  canActivate:[AuthGuard]
},
{
  path: 'mylist',
  component: MylistComponent,
  canActivate:[AuthGuard]
}
]


@NgModule({
  declarations: [
    HomeComponent,
    BookingComponent,
    MylistComponent
  ],
  imports: [
    RouterModule.forChild(routerConfig),
    SharedModule
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
  providers: [PaymentService,AuthService,AuthGuardService],
})
export class HomeModule { }
