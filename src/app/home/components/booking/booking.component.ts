import { Component, OnInit, AfterViewInit, ChangeDetectorRef, OnChanges, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { StorageService } from '../../../shared/services/storage.service';
import { PaymentService } from '../../../shared/services/payment.service';
import { environment } from './../../../../environments/environment';

declare var jQuery;
declare var moment;
declare var Chargebee
@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css']
})
export class BookingComponent implements OnInit, AfterViewInit {
  public bookingData: any;
  public rideDetails: any = {};
  public bookingForm;
  public cbInstance;
  public todayDate = new Date()

  public payment_status : string = ''
  public payment_process : boolean = false

  constructor(private activatedRoute: ActivatedRoute, private storageservice: StorageService, private router: Router, private paymentservice: PaymentService,private changeDetectorRef: ChangeDetectorRef) {
     
  
    // setTimeout(() => {
    //   this.changeDetectorRef.detectChanges()
    // }, 0);
    this.storageservice.getCommonEmitter().subscribe((data) => {
      if(data.type="payment_successful")
      this.saveBooking()
    })
  }

  ngOnInit() {
    this.bookingData = this.storageservice.getUserData()
    this.formGroupBulid()
    this.bookingData.departure = new Date()
    if (this.bookingData && !this.bookingData.ride.to && !this.bookingData.user.email) {
      this.router.navigate(['/aq-index.json'])
    }
  }
  ngAfterViewInit() {
    jQuery('#departuredatetime').daterangepicker({
      singleDatePicker: true,
      showDropdowns: true,
      minDate: new Date(),
      timePicker: true,
      opens: 'top',
      locale: {
        format: 'M/DD/YYYY hh:mm A'
      }
    }, (start, end, label) => {
      this.bookingData.departure = start.format('M/DD/YYYY hh:mm A')
    });
    
  }
  formGroupBulid() {
    let fg = {
      'name': new FormControl(this.bookingData.user.name, [Validators.required, Validators.minLength(3)]),
      'email': new FormControl(this.bookingData.user.email, [Validators.required, Validators.email]),
      'address': new FormControl(this.bookingData.address, [Validators.required, Validators.minLength(3)]),
      'phone': new FormControl(this.bookingData.user.phone, [Validators.required, Validators.minLength(10), Validators.maxLength(10)]),
    }
    this.bookingForm = new FormGroup(fg);
  }

  saveBooking() {
    this.payment_process = true
    this.paymentservice.saveBooking().subscribe((response) => {
      console.log(response)
      this.payment_process = false
      if (response.success) {
        this.payment_status = 'success'
        localStorage.removeItem('ridebooking')
       this.router.navigate(['/aq-index.json'])
       
      }else{
        this.payment_status = 'failed'
      }
     
    })
  }

  onSubmit() {
    if(this.bookingForm.valid && this.bookingData.departure){
      this.storageservice.getUserData().address = this.bookingForm.value.address
      this.storageservice.getUserData().user.name = this.bookingForm.value.name
      this.storageservice.getUserData().user.phone = this.bookingForm.value.phone
      this.storageservice.getUserData().departure = this.bookingData.departure
      this.payment_status = ''
      this.paymentservice.makePayment()
    }
  }

}
