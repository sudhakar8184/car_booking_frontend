import { Component, OnInit } from '@angular/core';
import { UserBaseService } from './../../../shared/services/user-base.service';

@Component({
  selector: 'app-mylist',
  templateUrl: './mylist.component.html',
  styleUrls: ['./mylist.component.css']
})
export class MylistComponent implements OnInit {

  constructor(public userbaseservice: UserBaseService) { }
  public myList:any;
  ngOnInit() {
    this.userbaseservice.getMyBookingsLIst().subscribe((response)=>{
      if(response.success){
        this.myList= response.data
      }
    })
  }

}
