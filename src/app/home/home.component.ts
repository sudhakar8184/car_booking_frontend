import { Component, OnInit, ViewChild, AfterViewInit, ChangeDetectorRef, NgZone } from '@angular/core';
import { LocationService } from '../shared/services/location.service';
import { UserBaseService } from '../shared/services/user-base.service';
import { Router } from '@angular/router';
import { StorageService } from '../shared/services/storage.service';
import { environment } from './../../environments/environment';
declare var google;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  constructor(public locationservice: LocationService, public userbaseservice: UserBaseService, private router: Router, private storageservice: StorageService,private changeDetectorRef: ChangeDetectorRef,
    public ngZone: NgZone) {
   }
  public rideData = {
    from: 'Bengaluru, Karnataka, India',
    to: '',
    distance: null,
    price: null,
    duration: ''
  }
  public destination=''
  public driversData: any = []
  distanceError: String = ''
  public language: any = '';
  public loading: Boolean = false
  @ViewChild('toaddress') toaddress: any;
  ngOnInit() {
    this.rideData = this.storageservice.getUserData().ride
    this.locationservice.runScript().subscribe((response) => {
      console.log(response)
      this.getPlaceAutocomplete()
        this.getuserDetails()
    })

  }

  getDriver() {
    if (Number(this.rideData.distance)) {
      this.userbaseservice.getDriver(this.language).subscribe((response) => {
        if (response.success) {
          this.loading = false
          this.driversData = response.data
          this.changeDetectorRef.detectChanges()
        }
      })
    }
  }
  getuserDetails() {
    if( this.storageservice.getSessionData('token'))
        this.userbaseservice.getUserDetails().subscribe((response) => {
          if (response.success) {
            this.storageservice.getUserData().user = response.data
            if (this.storageservice.getSessionData('ridebooking')) {
              this.destination= JSON.parse(this.storageservice.getSessionData('ridebooking')).to
              console.log(this.rideData)
              this.calculateDistance()
            }
          }
        })
  }
  getPlaceAutocomplete() {
    const autocomplete = new google.maps.places.Autocomplete(this.toaddress.nativeElement,
      {
        componentRestrictions: { country: 'IN' },
        types: ['geocode']
      });
    google.maps.event.addListener(autocomplete, 'place_changed', () => {
      this.ngZone.run(() => {
        const place = autocomplete.getPlace();
        this.destination = place.formatted_address;
      })
    });
  }
  bookingData(i?: any) {

    if (this.storageservice.getSessionData('token')) {
      this.rideData.price = this.getPrice(this.driversData[i].price)
      this.rideData['driver'] = this.driversData[i]
      this.storageservice.getUserData().ride = this.rideData
      this.storageservice.getUserData().departure = new Date()
      this.ngZone.run(() => this.router.navigate(['aq-index.json/booking', { ridebooking: JSON.stringify(this.rideData) }], { skipLocationChange: true, }));
     
    } else {
      this.rideData['driver'] = this.driversData[i]
      localStorage.setItem('ridebooking', JSON.stringify(this.rideData))
      window.location.href = environment.domain+"/login"
    }

  }

  calculateDistance() {
    this.rideData.to = this.destination
    if(this.rideData.to){
      this.loading = true
      let service = new google.maps.DistanceMatrixService();
      service.getDistanceMatrix(
        {
          origins: [this.rideData.from],
          destinations: [this.rideData.to],
          travelMode: google.maps.TravelMode.DRIVING,
          unitSystem: google.maps.UnitSystem.IMPERIAL,
          avoidHighways: false,
          avoidTolls: false
        }, (response, status) => {
          if (status != google.maps.DistanceMatrixStatus.OK) {
            // $('#result').html(err);
            this.distanceError = 'not fetch'
          } else {
            if (response.rows[0].elements[0].status === "ZERO_RESULTS") {
              this.distanceError = "Better get on a plane. There are no roads between " + this.rideData.from + " and " + this.rideData.to
            } else {
              console.log(response)
              let distance = response.rows[0].elements[0].distance;
              let duration = response.rows[0].elements[0].duration;
              distance = (distance.value / 1000)
              this.rideData.distance = distance// the kilom
              this.rideData.duration = duration.text;
              console.log(this.rideData)
            }
          }
          this.getDriver()
        });
    }
   
  }

  getPrice(price) {
    return Math.round(Number(price) * Number(this.rideData.distance)).toFixed(2)
  }


}
