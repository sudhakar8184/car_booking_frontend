export class User {
   public name: string;
   public email: string;
   public password: string;
   public phone: number;
   constructor(name: string, email: string, phone: number) {
      this.name = name;
      this.email = email;
      this.phone = phone;
   }
   public static create(): User {
      return new User(null, null, null);
   }
}