import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs/Observable';
declare var jQuery;
@Injectable()
export class LocationService {
  constructor(public router: Router) { }
  runScript(): Observable<any> {
    return new Observable((observer) => {
      jQuery.getScript(`https://maps.googleapis.com/maps/api/js?libraries=places&key=${environment.placeApiKey}`)
        .then((data) => {
          observer.next([{ loaded: true, status: 'Loaded' }]);
        });
    });
  }
}
