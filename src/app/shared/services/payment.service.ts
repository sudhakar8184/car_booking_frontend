import { Injectable } from "@angular/core";
import { environment } from '../../../environments/environment';
import { StorageService } from './storage.service';
import { ApiServiceService } from './api.service';
import { Observable } from 'rxjs/Observable';
import { Http, } from '@angular/http';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
declare var StripeCheckout: any;
declare var bootbox: any;

@Injectable()
export class PaymentService extends ApiServiceService {
    stripeHandler: any
    paymentStatus: string[] = [];
    controlId: any;
    constructor(public http: Http, public storageservice: StorageService) {
        super()
        this.configureStripe();
    }

    configureStripe() {
        this.stripeHandler = StripeCheckout.configure({
            key: environment.stripe,
            image: '',
            locale: 'auto',
            token: (token) => {
                this.storageservice.getUserData().stripeToken = token.id
                this.storageservice.commonEmitter.emit({ type: 'payment_successful'})
            },
            closed: () => {
                // console.log('payment popup closedddddd')
            }
        });
    }

    saveBooking(): Observable<any> {
        console.log(this.storageservice.getUserData())
        this.storageservice.getUserData().driver = this.storageservice.getUserData().ride['driver']._id
        return this.http.post(environment.root + 'savebooking', {
            data: this.storageservice.getUserData()
        }, this.post()).map((res) => {
            return res.json()
        }).catch((error) => {
            return new ErrorObservable(error.error);
        })
    }
    makePayment() {
        this.stripeHandler.open({
            name: 'Booking Sysytem',
            excerpt: 'Deposit Money to Account',
            currency: "INR",
            amount: Number(this.storageservice.getUserData().ride.price*100)
        });
    }
}